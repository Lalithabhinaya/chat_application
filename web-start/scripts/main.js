"use strict";

//Java Script file for USF Messenger detailed page

//Performs User authentication process.
function initFirebaseAuthorization() {
    firebase.auth().onAuthStateChanged(userAuthorizationState);
}

// Performs user signs-in and  signs-out functionalities.
function userAuthorizationState(sign_in) {
    if (sign_in) {
        // Verifies the user status:if signed in then retrieve user details
        var userPic = getUserPic();
        var profileName = getLoggedInUserName();

        // Retrieve user's display picture and name
        userPicElement.style.backgroundImage = 'url(' + userPic + ')';
        userNameElement.textContent = profileName;

        // If User sign-in is successful then make below items visible on page
        userNameElement.removeAttribute('hidden');
        userPicElement.removeAttribute('hidden');
        logOutButton.removeAttribute('hidden');

        // Post sign-in, make log-in button invisible
        logInButton.setAttribute('hidden', 'true');

    } else {
        // Signed-out operation
        userNameElement.setAttribute('hidden', 'true');
        userPicElement.setAttribute('hidden', 'true');
        logOutButton.setAttribute('hidden', 'true');

        // Show sign-in button.
        logInButton.removeAttribute('hidden');
    }
}

// Fetch user's profile image
function getUserPic() {
    return firebase.auth().currentUser.photoURL;
}

// performs Sign-in
function Messenger_signIn() {
    // Sign-in with Google provider
    var Google_provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(Google_provider);
}

// Fetch User's display Name
function getLoggedInUserName() {
    return firebase.auth().currentUser.displayName;
}

// Performs Sign-out.
function Messenger_signOut() {
    firebase.auth().signOut();
    //Post sign-out navigate to home page.
    window.location.replace("index.html");
}

//Validates firebase setup
function validSetup() {
    if (!firebase.app().options ||!window.firebase  ||!(firebase.app instanceof Function) ) {
        window.alert('Configure firebase properly');
    }
}

// Retrieves previous messages from database and listen for new messages
function loadChatMessages() {
    var temp;
    temp = function(snap) {
        var element;
        element = snap.val();
        smsElement(snap.key, element.name, element.text, element.profilePicUrl, element.imageUrl);
    };

    firebase.database().ref('/messages/').limitToLast(12).on('child_added', temp);
    firebase.database().ref('/messages/').limitToLast(12).on('child_changed', temp);
}

// push old messages to database
function saveChatMessage(userInput) {
    // Push messages to database
    return firebase.database().ref('/messages/').push({
        name: getLoggedInUserName(),
        text: userInput,
        profilePicUrl: getUserPic()
    }).catch(function(error) {
        console.error('Error while loading input message to database', error);
    });
}


// Verifies user login and returns true or false boolean value
function isUserLoggedIn() {
    return !!firebase.auth().currentUser;
}


// save pictures to database
function savePicture(Image) {
    firebase.database().ref('/messages/').push({
        name: getLoggedInUserName(),
        imageUrl: SPIN_URL,
        profilePicUrl: getUserPic()
    }).then(function(messageRef) {
        var filePath = firebase.auth().currentUser.uid + '/' + messageRef.key + '/' + Image.name;
        return firebase.storage().ref(filePath).put(Image).then(function(fileSnapshot) {
            return fileSnapshot.ref.getDownloadURL().then((url) => {
                return messageRef.update({
                    imageUrl: url,
                    storageUri: fileSnapshot.metadata.fullPath
                });
            });
        });
    }).catch(function(error) {
        console.error('Error while uploading Image:', error);
    });
}


// Check whether user signed-in or not
function checkSignedInWithMessage() {
    if (isUserLoggedIn()) {
        return true;
    }
    return false;
}

//Reset cursor on message bar
function resetSendIcon(element) {
    element.value = '';
    element.parentNode.MaterialTextfield.boundUpdateClassesHandler();
}


// Activates when user clicks submit button
function smsSubmitForm(e) {
    e.preventDefault();
    if (smsAvailEvent.value) {
        saveChatMessage(smsAvailEvent.value).then(function() {
            resetSendIcon(smsAvailEvent);
            resetSmsBar();
        });
    }
}

// Activates when user clicks on upload image
function picUploadEvent(event) {
  event.preventDefault();
  var media;
    media= event.target.files[0];
    pictureFormEvent.reset();
    if (checkSignedInWithMessage()) {
        savePicture(media);
    }

}

// Text Message Format
var SMS_FORMAT =
    '<div class="message-container">' +
    '<div class="spacing"><div class="pic"></div></div>' +
    '<div class="message"></div>' +
    '<div class="name"></div>' +
    '</div>';


//Spinning image
var SPIN_URL = 'https://www.google.com/images/spin-32.gif?a';

// Functionality on send button:activates and deactivates send button based on the text literal.
function resetSmsBar() {
    if (smsAvailEvent.value) {
        submitEvent.removeAttribute('disabled');
    } else {
        submitEvent.setAttribute('disabled', 'true');
    }
}


// Message format display
function smsElement(key, name, text, picUrl, imageUrl) {
  var element = document.getElementById(key);
  if (!element) {
    var container;
      container = document.createElement('element');
    container.innerHTML = SMS_FORMAT;
      element = container.firstChild;
      element.setAttribute('id', key);
      smsEvent.appendChild(element);
  }
  if (picUrl) {
      element.querySelector('.pic').style.backgroundImage = 'url(' + picUrl + ')';
  }
    element.querySelector('.name').textContent = name;
  var messageElement;
    messageElement = element.querySelector('.message');
  if (text) {
    messageElement.textContent = text;

    messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, '<br>');
  } else if (imageUrl) {
    var pic;
      pic = document.createElement('img');
      pic.addEventListener('load', function() {
        smsEvent.scrollTop = smsEvent.scrollHeight;
    });
      pic.src = imageUrl + '&' + new Date().getTime();
    messageElement.innerHTML = '';
    messageElement.appendChild(pic);
  }

  setTimeout(function() {element.classList.add('visible')}, 1);
    smsEvent.scrollTop = smsEvent.scrollHeight;
    smsAvailEvent.focus();
}


validSetup();


var logOutButton = document.getElementById('log-out');
var smsEvent = document.getElementById('smss');
var logInButton = document.getElementById('log-in');
var smsTypingEvent = document.getElementById('sms-form');
var submitEvent = document.getElementById('submit');
var pictureEvent = document.getElementById('SendPicture');
var smsAvailEvent = document.getElementById('sms');
var pictureFormEvent = document.getElementById('picture');
var userPicElement = document.getElementById('profile-pic');
var uploadEvent = document.getElementById('upload');
var userNameElement = document.getElementById('profile-name');


logInButton.addEventListener('click', Messenger_signIn);
logOutButton.addEventListener('click', Messenger_signOut);

if (smsAvailEvent){
    smsAvailEvent.addEventListener('keyup', resetSmsBar);
    smsAvailEvent.addEventListener('change', resetSmsBar);
}

if (pictureEvent) {
    pictureEvent.addEventListener('click', function(e) {
        e.preventDefault();
        uploadEvent.click();
    });
    uploadEvent.addEventListener('change', picUploadEvent);
}

if (smsTypingEvent){
    smsTypingEvent.addEventListener('submit', smsSubmitForm);
}


initFirebaseAuthorization();

loadChatMessages();