"use strict";
//Java Script file for Home page

//Performs User authentication process.
function initFirebaseAuthorization() {
    firebase.auth().onAuthStateChanged(userAuthorizationState);
}

// Performs user signs-in and  signs-out functionalities.
function userAuthorizationState(sign_in) {
    // Verifies the user status:if signed in then retrieve user details
    if (sign_in) {
        var userPic = getUserPic();
        var profileName = getLoggedInUserName();

        // Retrieve user's display picture and name
        userPicElement.style.backgroundImage = 'url(' + userPic + ')';
        userNameElement.textContent = profileName;

        // If User sign-in is successful then make below items visible on page
        userNameElement.removeAttribute('hidden');
        userPicElement.removeAttribute('hidden');
        logOutButton.removeAttribute('hidden');

        // Post sign-in, make log-in button invisible
        logInButton.setAttribute('hidden', 'true');
        //upon successful login,redirect to message screen html page.
        window.location.replace("index1.html");

    } else {
        // Signed-out operation
        userNameElement.setAttribute('hidden', 'true');
        userPicElement.setAttribute('hidden', 'true');
        logOutButton.setAttribute('hidden', 'true');

        // Show sign-in button.
        logInButton.removeAttribute('hidden');
    }
}

// Fetch user's profile image
function getUserPic() {
    return firebase.auth().currentUser.photoURL;
}


// performs Sign-in
function Messenger_signIn() {
    // Sign-in with Google provider
    var Google_provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(Google_provider);
}

// Fetch User's display Name
function getLoggedInUserName() {
    return firebase.auth().currentUser.displayName;
}

// Performs Sign-out.
function Messenger_signOut() {
    firebase.auth().signOut();

}


//Validates firebase setup
function validSetup() {
    if (!firebase.app().options ||!window.firebase  ||!(firebase.app instanceof Function) ) {
        window.alert('Configure firebase properly');
    }
}

validSetup();


var logInButton = document.getElementById('log-in');
var userPicElement = document.getElementById('profile-pic');
var userNameElement = document.getElementById('profile-name');
var logOutButton = document.getElementById('log-out');

logInButton.addEventListener('click', Messenger_signIn);
logOutButton.addEventListener('click', Messenger_signOut);


// Triggers firebase Authorization
initFirebaseAuthorization();
